%if 0%{?fedora} || 0%{?rhel} >= 7
%global with_python3 1
%else
%global with_python3 0
%endif
%global srcname scikit-image

Name: python-scikit-image
Version: 0.12.3
Release: 5.2%{?dist}
Summary: Image processing in Python
# The following files are BSD 2 clauses, the rest BSD 3 clauses
# skimage/graph/_mcp.pyx
# skimage/graph/heap.pyx
License: BSD

URL: http://scikit-image.org/
Source0: https://pypi.python.org/packages/source/s/scikit-image/scikit-image-%{version}.tar.gz
Patch0: https://patch-diff.githubusercontent.com/raw/scikit-image/scikit-image/pull/2036.patch

BuildRequires: xorg-x11-server-Xvfb

%description
The scikit-image SciKit (toolkit for SciPy) extends scipy.ndimage to provide a 
versatile set of image processing routines.

%package -n python%{python3_pkgversion}-%{srcname}
Summary: Image processing in Python 3
BuildRequires: python%{python3_pkgversion}-devel python%{python3_pkgversion}-setuptools python%{python3_pkgversion}-numpy
BuildRequires: python%{python3_pkgversion}-scipy python%{python3_pkgversion}-matplotlib python%{python3_pkgversion}-nose
BuildRequires: python%{python3_pkgversion}-six >= 1.3
BuildRequires: python%{python3_pkgversion}-networkx-core
BuildRequires: python%{python3_pkgversion}-pillow
Requires: python%{python3_pkgversion}-scipy
Requires: python%{python3_pkgversion}-six >= 1.3
Requires: python%{python3_pkgversion}-networkx-core
Requires: python%{python3_pkgversion}-pillow
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

%description -n python%{python3_pkgversion}-%{srcname}
The scikit-image SciKit (toolkit for SciPy) extends scipy.ndimage to provide a 
versatile set of image processing routines.

%package -n %{srcname}-tools
Summary: Scikit-image utility tools
BuildArch: noarch
Requires: python%{python3_pkgversion}-%{srcname} = %{version}-%{release}

%description -n %{srcname}-tools
Utilities provided by scikit-image: 'skivi'

%prep
%setup -n %{srcname}-%{version} -q
%patch0 -p1 -b .Cython
# Remove some shebangs
pushd skimage
for i in $(grep -l -r "/usr/bin/env"); do
   sed -i -e '1d' $i;
done
popd

%build
%py3_build

%install
%py3_install

find %{buildroot} -name "*.so" | xargs chmod 755

# Checks are not working at the moment
%check
# Fake matplotlibrc
mkdir -p matplotlib
touch matplotlib/matplotlibrc
export XDG_CONFIG_HOME=`pwd`
pushd %{buildroot}/%{python3_sitearch}
xvfb-run nosetests-%{python3_version} skimage || :
popd
 
%files -n python%{python3_pkgversion}-%{srcname}
%doc CONTRIBUTORS.txt DEPENDS.txt RELEASE.txt TASKS.txt
%license LICENSE.txt
%{python3_sitearch}/skimage
%{python3_sitearch}/scikit_image-*.egg-info


%files -n %{srcname}-tools
%{_bindir}/skivi

%changelog
* Fri Jan 31 2020 Michael Thomas <michael.thomas@LIGO.ORG> - 0.13.0-5.2
- Drop python2, python3.4 support

* Mon Apr 08 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.13.0-5.1
- Add python3.6 subpackage

* Tue Aug 16 2016 Orion Poplawski <orion@cora.nwra.com> - 0.12.3-5
- Remove Cython build requirement
- Only build python3 for Fedora

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.12.3-4
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Wed Apr 6 2016 Orion Poplawski <orion@cora.nwra.com> - 0.12.3-3
- Run tests, but do not abort build on failure
- Drop py3dir, use new python macros

* Tue Mar 29 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.12.3-2
- New upstream source
- Disable tests for the moment

* Fri Feb 19 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.11.3-8
- skivi uses python3 (bz #1309240)
- Provides "versioned" python2-scikit-image

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.11.3-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Jan 05 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.11.3-6
- Tarball without problematic copyrigth images (fixes bz #1295193)

* Mon Nov 23 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.11.3-5
- Provides python2-scikit-image

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11.3-4
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu May 21 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.11.3-2
- Disable tests
- New upstream version (0.11.3)

* Thu Mar 12 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.11.2-1
- New upstream version (0.11.2)

* Wed Feb 04 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.11.0-1
- New upstream version

* Tue Jul 29 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.10.1-2
- Remove __provides_exclude_from, is not needed in f20+

* Wed Jul 09 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.10.1-1
- New upstream 0.10.1

* Thu Apr 18 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.9.3-1
- Initial spec file

